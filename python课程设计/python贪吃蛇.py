#!/usr/bin/env python
import pygame,sys,time,random
from pygame.locals import *

# 定义颜色变量
redColour = pygame.Color(255,0,0) # 定义颜色
blackColour = pygame.Color(0,0,0) # 背景颜色
whiteColour = pygame.Color(255,255,255) # 贪吃蛇颜色
greyColour = pygame.Color(150,150,150)
LightGrey = pygame.Color(220,220,220)

# 游戏窗口的大小
windows_width = 800
windows_height = 600
cell_size = 20 #贪吃蛇身体方块大小,注意身体大小必须能被窗口长宽整除
# 初始化区：由于我们的贪吃蛇是有大小尺寸的, 因此地图的实际尺寸是相对于贪吃蛇的大小尺寸而言的
map_width = int(windows_width / cell_size)
map_height = int(windows_height / cell_size)

# 定义gameOver函数
def gameOver(playSurface,i):
    gameOverFont = pygame.font.Font('arial.ttf',72)
    gameOverSurf = gameOverFont.render('Game Over', True, redColour)
    gameover = pygame.image.load('gameover.jpg')
    playSurface.blit(gameover, (75, 80))
    gameOverRect = gameOverSurf.get_rect()
    gameOverRect.midtop = (400, 150)
    playSurface.blit(gameOverSurf, gameOverRect)
    gameOverSurf1 = gameOverFont.render('Your Score:%d'%i, True, redColour) #score
    gameOverRect1 = gameOverSurf1.get_rect()
    gameOverRect1.midtop = (400, 250)
    playSurface.blit(gameOverSurf1, gameOverRect1)
    pygame.display.flip() # 刷新显示界面
    while True: # 键盘监听事件
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate() # 终止程序
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_q: # 终止程序
                    terminate() # 终止程序
# 程序终止
def terminate():
	pygame.quit()
	sys.exit()

# 定义main函数
def main(i,k):
    # 初始化pygame
    pygame.init()

    # 定义变量控制游戏速度：创建Pygame时钟对象
    fpsClock = pygame.time.Clock()
    
    # 创建pygame显示层
    playSurface = pygame.display.set_mode((windows_width, windows_height))
    pygame.display.set_caption('Python 贪吃蛇小游戏') # 设置标题

    # 初始化变量
    snakePosition = [100,100] # 蛇头位置
    snakeSegments = [[100,100],[80,100],[60,100]] # 初始长度为3个单位，初始化贪吃蛇长度，列表中有几个元素就有几段身体
    raspberryPosition = [300,300] # 初始化目标方块位置
    raspberrySpawned = 1 # 树莓个数
    direction = 'right' # 初始方向
    changeDirection = direction # 定义一个方向变量（人为控制）
    score = 0 # 初始分数
    while True:
        # 检测例如按键等pygame事件
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                # 判断键盘事件
                if event.key == K_RIGHT or event.key == ord('d'):
                    changeDirection = 'right'
                if event.key == K_LEFT or event.key == ord('a'):
                    changeDirection = 'left'
                if event.key == K_UP or event.key == ord('w'):
                    changeDirection = 'up'
                if event.key == K_DOWN or event.key == ord('s'):
                    changeDirection = 'down'
                if event.key == K_ESCAPE:
                    pygame.event.post(pygame.event.Event(QUIT))

        # 判断是否输入了反方向
        if changeDirection == 'right' and not direction == 'left':
            direction = changeDirection
        if changeDirection == 'left' and not direction == 'right':
            direction = changeDirection
        if changeDirection == 'up' and not direction == 'down':
            direction = changeDirection
        if changeDirection == 'down' and not direction == 'up':
            direction = changeDirection

        # 根据方向移动蛇头的坐标
        if direction == 'right':
            snakePosition[0] += 20
        if direction == 'left':
            snakePosition[0] -= 20
        if direction == 'up':
            snakePosition[1] -= 20
        if direction == 'down':
            snakePosition[1] += 20

        # 增加蛇的长度
        snakeSegments.insert(0,list(snakePosition))

        # 判断是否吃掉了树莓
        if snakePosition[0] == raspberryPosition[0] and snakePosition[1] == raspberryPosition[1]:
            raspberrySpawned = 0
            i+=1
            if i%3==0:
                k+=1
        else:
            snakeSegments.pop()

        # 如果吃掉树莓，则重新生成树莓
        if raspberrySpawned == 0:
            x = random.randrange(1,32)
            y = random.randrange(1,24)
            raspberryPosition = [int(x*20),int(y*20)] 
            raspberrySpawned = 1

        # 绘制pygame显示层，填充背景颜色
        background = pygame.image.load('background.jpg').convert()
        playSurface.blit(background,(0,0))
        for position in snakeSegments[1:]:
            pygame.draw.rect(playSurface,whiteColour,Rect(position[0],position[1],20,20)) # 蛇身为白色
            pygame.draw.rect(playSurface,LightGrey,Rect(snakePosition[0],snakePosition[1],20,20)) # 蛇头为灰色
            pygame.draw.rect(playSurface,redColour,Rect(raspberryPosition[0], raspberryPosition[1],20,20)) # 树莓为红色
            # 第一个参数surface:指定一个编辑区
            # 第二个参数color:颜色
            # 第三个参数填充 Rect:返回矩形(（x,y）,(width,height))
            # 第四个参数width:表示线条粗细 width=0 填充  width=1 实心
        # 刷新pygame显示层
        pygame.display.update()

        # 判断是否死亡
        if snakePosition[0] > windows_width-cell_size or snakePosition[0] < 0:
            gameOver(playSurface,i)
        if snakePosition[1] > windows_height-cell_size or snakePosition[1] < 0:
            gameOver(playSurface,i)
        for snakeBody in snakeSegments[1:]: # 蛇碰到自己身体
            if snakePosition[0] == snakeBody[0] and snakePosition[1] == snakeBody[1]:
                gameOver(playSurface,i)
           
        # 控制游戏速度
        if k==0:
            fpsClock.tick(3)
        if k==1:
            fpsClock.tick(4)
        if k==2:
            fpsClock.tick(5)
        if k==3:
            fpsClock.tick(6)
        if k==4:
            fpsClock.tick(7)
        if k==5:
            fpsClock.tick(8)
        if k==6:
            fpsClock.tick(9)
        if k==7:
            fpsClock.tick(10)
        if k==8:
            fpsClock.tick(11)
        if k==9:
            fpsClock.tick(12)
        if k==10:
            fpsClock.tick(13)
        if k==11:
            fpsClock.tick(14)
        if k>=12:
            fpsClock.tick(15)
        

k=int(input("请输入初始速度倍率：（0：初始速度，3：2倍速，6：3倍速，9：4倍速）"))
main(0,k)
